package mad.citybin.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import mad.citybin.R
import mad.citybin.models.Driver
import mad.citybin.ui.drivers.DriverDetailActivity

class DriverListAdapter(
    private var drivers: MutableList<Driver> = MutableList(0) { Driver() }
) : RecyclerView.Adapter<DriverListAdapter.DriverViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverViewHolder =
        DriverViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_driver, parent, false)
        )

    override fun getItemCount(): Int = drivers.size

    override fun onBindViewHolder(holder: DriverViewHolder, position: Int) {
        holder.bind(drivers[position])
    }

    fun loadData(drivers: MutableList<Driver>) {
        this.drivers = drivers
        notifyDataSetChanged()
    }

    inner class DriverViewHolder(
        itemView: View,
        private val driverName: TextView = itemView.findViewById(R.id.driverName),
        private val driverProfileImg: SimpleDraweeView = itemView.findViewById(R.id.driverProfileImg)
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(driver: Driver) {
            driverName.text = driver.name
            driverProfileImg.setImageURI(driver.profileImgUrl, itemView.context)
            itemView.setOnClickListener {
                val context = it.context
                val intent = Intent(context, DriverDetailActivity::class.java)
                intent.putExtra("driver", driver)
                context.startActivity(intent)
            }
        }

    }
}