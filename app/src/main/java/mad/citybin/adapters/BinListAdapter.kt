package mad.citybin.adapters

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import mad.citybin.R
import mad.citybin.models.Bin
import mad.citybin.ui.bins.BinDetailActivity

class BinListAdapter(
    private var bins: MutableList<Bin> = MutableList(0) { Bin() }
) : RecyclerView.Adapter<BinListAdapter.BinViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BinViewHolder {
        return BinViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_bin, parent, false)
        )
    }

    override fun getItemCount(): Int = bins.size

    override fun onBindViewHolder(holder: BinViewHolder, position: Int) {
        holder.bind(bins[position])
    }

    fun loadData(bins: MutableList<Bin>) {
        this.bins = bins
        notifyDataSetChanged()
    }

    inner class BinViewHolder(
        itemView: View,
        private val binName: TextView = itemView.findViewById(R.id.binName),
        private val binImg: SimpleDraweeView = itemView.findViewById(R.id.binImg)
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(bin: Bin) {
            binName.text = bin.name
            binImg.setImageURI(Uri.parse(bin.imgUrl), itemView.context)
            itemView.setOnClickListener {
                val context = it.context
                val intent = Intent(context, BinDetailActivity::class.java)
                intent.putExtra("bin", bin)
                context.startActivity(intent)
            }
        }

    }

}