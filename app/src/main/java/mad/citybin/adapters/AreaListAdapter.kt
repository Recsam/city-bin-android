package mad.citybin.adapters

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import mad.citybin.R
import mad.citybin.models.Area
import mad.citybin.ui.bins.BinAreaDetailActivity

class AreaListAdapter(
    private var areas: MutableList<Area> = MutableList(0) { _ -> Area() }
) : RecyclerView.Adapter<AreaListAdapter.AreaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreaViewHolder =
        AreaViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_bin_area, parent, false)
        )

    override fun getItemCount(): Int = areas.size

    override fun onBindViewHolder(holder: AreaViewHolder, position: Int) {
        holder.bind(areas[position])
    }

    fun loadData(areas: MutableList<Area>) {
        this.areas = areas
        notifyDataSetChanged()
    }

    inner class AreaViewHolder(
        itemView: View,
        private val image: SimpleDraweeView = itemView.findViewById(R.id.binAreaImage),
        private val name: TextView = itemView.findViewById(R.id.binAreaName)
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(area: Area) {
            image.setImageURI(Uri.parse(area.imageUrl), itemView.context)
            name.text = area.name
            itemView.setOnClickListener {
                val context = it.context
                val intent = Intent(context, BinAreaDetailActivity::class.java)
                intent.putExtra("bin-area", area)
                context.startActivity(intent)
            }
        }

    }

}