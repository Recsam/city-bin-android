package mad.citybin.models

import java.io.Serializable
import java.util.*

data class Driver(
    var name: String = "",
    var binsCollected: Int = 0,
    var tel: String = "",
    var profileImgUrl: String? = null,

    var id: String = UUID.randomUUID().toString()
) : Serializable