package mad.citybin.models

import java.io.Serializable
import java.util.*

data class Bin(
    var name: String = "",
    var description: String = "",
    var isCleared: Boolean = false,
    var imgUrl: String = "",
    var area: Area = Area(),

    var id: String = UUID.randomUUID().toString()
) : Serializable