package mad.citybin.models

import java.util.*

data class User (
    var username: String = "",
    var email: String = "",
    var password: String = "",

    var id: String = UUID.randomUUID().toString()
)