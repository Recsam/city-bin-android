package mad.citybin.models

import java.io.Serializable
import java.util.*

data class Area(
    var name: String = "",
    var imageUrl: String = "",
    var description: String = "",
    var bins: MutableList<Bin> = mutableListOf(),

    var id: String = UUID.randomUUID().toString()
) : Serializable