package mad.citybin.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_home.*
import mad.citybin.R
import mad.citybin.services.PrefDataManager
import mad.citybin.ui.auth.SignInActivity
import mad.citybin.ui.auth.SignUpActivity

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (PrefDataManager(this).getUser() != null) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        if (intent?.getBooleanExtra("isLoggedOut", false)!!) {
            Snackbar.make(findViewById(android.R.id.content), "Logged out successfully!", Snackbar.LENGTH_SHORT).show()
        }
        setContentView(R.layout.activity_home)

        btnSignIn.setOnClickListener {
            startActivityForResult(Intent(this, SignInActivity::class.java), 1)
        }

        btnSignUp.setOnClickListener {
            startActivityForResult(Intent(this, SignUpActivity::class.java), 2)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                1, 2 -> finish()
            }
        }
    }

}