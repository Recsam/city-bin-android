package mad.citybin.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_bin_detail.*
import kotlinx.android.synthetic.main.activity_bin_detail.btnBack
import kotlinx.android.synthetic.main.activity_profile.*
import mad.citybin.R
import mad.citybin.services.PrefDataManager
import mad.citybin.services.UserService

class ProfileActivity : AppCompatActivity() {
    lateinit var prefDataManager: PrefDataManager
    lateinit var userService: UserService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        prefDataManager = PrefDataManager(this)
        userService = UserService(this)

        userService.getById("${prefDataManager.getUser()}") { user ->
            username.text = user.username
            email.text = user.email
        }

        btnBack.setOnClickListener { finish() }
    }
}