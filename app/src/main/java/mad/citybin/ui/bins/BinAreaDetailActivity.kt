package mad.citybin.ui.bins

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_bin_area_detail.*
import mad.citybin.R
import mad.citybin.adapters.BinListAdapter
import mad.citybin.models.Area
import mad.citybin.services.AreaService
import mad.citybin.services.BinService
import mad.citybin.utils.Tools
import mad.citybin.utils.ViewAnimation

class BinAreaDetailActivity : AppCompatActivity() {
    private lateinit var areaService: AreaService
    private lateinit var binService: BinService
    private val binListAdapter = BinListAdapter()
    private var area = Area()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bin_area_detail)

        areaService = AreaService(this)
        binService = BinService(this)
        area = intent.extras?.get("bin-area") as Area

        swipeRefresh.setColorSchemeColors(Color.rgb(76, 175, 80))
        swipeRefresh.setOnRefreshListener { loadData() }

        btnBack.setOnClickListener { finish() }

        setupUI()

        swipeRefresh.isRefreshing = true
        loadData()

        btnToggleText.setOnClickListener { toggleSelectionText(btnToggleText) }
        btnHideText.setOnClickListener { toggleSelectionText(btnToggleText) }

    }

    private fun setupUI() {
        binsList.layoutManager = LinearLayoutManager(this)
        binsList.adapter = binListAdapter

        showData()
    }

    private fun showData() {
        binAreaName.text = area.name
        binAreaDescription.text = area.description
        imgBinArea.setImageURI(Uri.parse(area.imageUrl), this)
    }

    private fun loadData() {
        areaService.getById(area.id) { area ->
            this.area = area
            showData()
        }
        binService.getAllWithAreaId(area.id) { bins ->
            binListAdapter.loadData(bins.toMutableList())
            binAmount.text = "${bins.size}"
            swipeRefresh.isRefreshing = false
        }
    }

    private fun toggleSelectionText(view: View) {
        val show = toggleArrow(view)
        if (show) {
            ViewAnimation.expand(layoutExpandText) { Tools.nestedScrollTo(nestedScrollView, layoutExpandText) }
        } else {
            ViewAnimation.collapse(layoutExpandText)
        }
    }

    private fun toggleArrow(view: View): Boolean {
        return if (view.rotation == 0.0F) {
            view.animate().setDuration(200).rotation(180.0F)
            true
        } else {
            view.animate().setDuration(200).rotation(0.0F)
            false
        }
    }

}