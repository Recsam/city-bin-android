package mad.citybin.ui.bins

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bin_detail.*
import kotlinx.android.synthetic.main.activity_bin_detail.binImg
import mad.citybin.R
import mad.citybin.models.Bin

class BinDetailActivity : AppCompatActivity() {
    private var bin = Bin()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bin_detail)

        bin = intent.extras?.get("bin") as Bin

        btnBack.setOnClickListener { finish() }

        setupUI()

    }

    private fun setupUI() {
        binName.text = bin.name
        binDescription.text = bin.description
        binImg.setImageURI(Uri.parse(bin.imgUrl), this)
    }
}