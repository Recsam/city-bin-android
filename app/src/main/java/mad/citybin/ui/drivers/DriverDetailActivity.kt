package mad.citybin.ui.drivers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bin_detail.*
import kotlinx.android.synthetic.main.activity_bin_detail.btnBack
import kotlinx.android.synthetic.main.activity_driver_detail.*
import mad.citybin.R
import mad.citybin.models.Bin
import mad.citybin.models.Driver

class DriverDetailActivity : AppCompatActivity() {
    private var driver = Driver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_detail)

        driver = intent.extras?.get("driver") as Driver

        btnBack.setOnClickListener { finish() }

        setupUI()
    }

    private fun setupUI() {
        driverName.text = driver.name
        driverTel.text = driver.tel
        driverCollected.text = getString(R.string.amount_bins, driver.binsCollected)
        driverProfileImg.setImageURI(driver.profileImgUrl, this)
    }
}