package mad.citybin.ui.auth

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_home.btnSignIn
import kotlinx.android.synthetic.main.activity_sign_in.*
import mad.citybin.R
import mad.citybin.services.PrefDataManager
import mad.citybin.services.UserService
import mad.citybin.ui.MainActivity

class SignInActivity : AppCompatActivity() {
    private lateinit var prefDataManager: PrefDataManager
    private lateinit var userService: UserService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        prefDataManager = PrefDataManager(this)
        userService = UserService(this)

        btnBack.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        btnSignIn.setOnClickListener {
            userService.getByEmail(edEmail.text.toString()) {user ->
                if (user != null && user.password == edPassword.text.toString()) {
                    prefDataManager.setUser(user.id)
                    startActivity(Intent(this, MainActivity::class.java))
                    setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    Snackbar.make(it, "Incorrect credentials!", Snackbar.LENGTH_SHORT).show()
                }

            }

        }
    }

}