package mad.citybin.ui.auth

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.btnBack
import kotlinx.android.synthetic.main.activity_sign_up.edEmail
import kotlinx.android.synthetic.main.activity_sign_up.edPassword
import mad.citybin.R
import mad.citybin.models.User
import mad.citybin.services.PrefDataManager
import mad.citybin.services.UserService
import mad.citybin.ui.MainActivity

class SignUpActivity: AppCompatActivity() {
    lateinit var prefDataManager: PrefDataManager
    lateinit var userService: UserService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        prefDataManager = PrefDataManager(this)
        userService = UserService(this)

        btnBack.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        btnSignUp.setOnClickListener {
            userService.store(User(
                edUsername.text.toString(),
                edEmail.text.toString(),
                edPassword.text.toString()
            )) {user ->
                if (user != null) {
                    prefDataManager.setUser(user.id)
                    startActivity(Intent(this, MainActivity::class.java))
                    setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    Snackbar.make(it, "Error, Unable to create account", Snackbar.LENGTH_SHORT).show()
                }
            }
        }

    }

}