package mad.citybin.ui.main

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import mad.citybin.R
import mad.citybin.services.PrefDataManager
import mad.citybin.ui.HomeActivity
import mad.citybin.ui.ProfileActivity

class SettingsFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = "Settings"
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        // Logout
        view.findViewById<View>(R.id.logout).setOnClickListener {
            val context = view.context
            PrefDataManager(context).removeUser()
            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra("isLoggedOut", true)
            context.startActivity(intent)
            activity?.finish()
        }

        view.findViewById<View>(R.id.btnReport).setOnClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setMessage("This functionality is not completed")
                .setPositiveButton("Report") { dialog, _ -> dialog.dismiss() }
                .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            builder.create().show()
        }

        view.findViewById<View>(R.id.btnViewProfile).setOnClickListener {
            val context = view.context
            context.startActivity(Intent(context, ProfileActivity::class.java))
        }

        return view
    }

}