package mad.citybin.ui.main

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import mad.citybin.R
import mad.citybin.services.BinService
import mad.citybin.services.PrefDataManager
import mad.citybin.services.UserService
import mad.citybin.ui.MainActivity


class HomeFragment : Fragment() {
    private lateinit var prefDataManager: PrefDataManager
    private lateinit var userService: UserService
    private lateinit var binService: BinService

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = "Home"
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        prefDataManager = PrefDataManager(context!!)

        userService = UserService(context)
        binService = BinService(context)

        val username = view.findViewById<TextView>(R.id.txtUsername)
        val totalBin = view.findViewById<TextView>(R.id.amountBinTotal)
        val unclearedBin = view.findViewById<TextView>(R.id.amountBinUncleared)

        userService.getById("${prefDataManager.getUser()}") { user ->
            username.text = user.username
        }

        binService.getAll { bins -> totalBin.text = bins.size.toString() }
        binService.getUncleared { amount -> unclearedBin.text = "($amount uncleared)" }

        view.findViewById<View>(R.id.btnBins).setOnClickListener {
            (activity as MainActivity).setPage(1)
        }

        view.findViewById<View>(R.id.btnComplain).setOnClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setMessage("This functionality is not completed")
                .setPositiveButton("Complain") { dialog, _ -> dialog.dismiss() }
                .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            builder.create().show()
        }

        val swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
        }

        return view
    }

}