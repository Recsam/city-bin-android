package mad.citybin.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import mad.citybin.R
import mad.citybin.adapters.DriverListAdapter
import mad.citybin.services.DriverService


class DriversFragment : Fragment() {
    private lateinit var driverService: DriverService
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private val driverListAdapter = DriverListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = "Drivers"
        val view = inflater.inflate(R.layout.fragment_drivers, container, false)

        driverService = DriverService(context)

        swipeRefresh = view.findViewById(R.id.swipeRefresh)
        val driverList = view.findViewById<RecyclerView>(R.id.driverList)
        driverList.layoutManager = LinearLayoutManager(context)
        driverList.adapter = driverListAdapter

        swipeRefresh.isRefreshing = true
        loadData()

        swipeRefresh.setOnRefreshListener { loadData() }

        return view
    }

    private fun loadData() {
        driverService.getAll { drivers ->
            driverListAdapter.loadData(drivers.toMutableList())
            swipeRefresh.isRefreshing = false
        }
    }

}