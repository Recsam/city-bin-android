package mad.citybin.ui.main

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import mad.citybin.R
import mad.citybin.adapters.AreaListAdapter
import mad.citybin.services.AreaService
import mad.citybin.services.BinService


class BinsFragment : Fragment() {
    private lateinit var areaService: AreaService
    private lateinit var binService: BinService
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var uncleared: TextView
    private val areaListAdapter = AreaListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = "Bins"
        val view = inflater.inflate(R.layout.fragment_bins, container, false)

        areaService = AreaService(context)
        binService = BinService(context)

        uncleared = view.findViewById(R.id.uncleared)

        swipeRefresh = view.findViewById(R.id.swipeRefresh)
        swipeRefresh.setColorSchemeColors(Color.rgb(76, 175, 80))
        swipeRefresh.setOnRefreshListener { loadData() }

        val areaList = view.findViewById<RecyclerView>(R.id.areaList)
        areaList.layoutManager = GridLayoutManager(context, 2)
        areaList.adapter = areaListAdapter

        swipeRefresh.isRefreshing = true
        loadData()

        return view
    }

    private fun loadData() {
        binService.getUncleared { amount ->
            uncleared.text = context?.getString(R.string.amount_bins, amount)
        }
        areaService.getAll { areas ->
            areaListAdapter.loadData(areas.toMutableList())
            swipeRefresh.isRefreshing = false
        }
    }

}