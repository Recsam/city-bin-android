package mad.citybin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.fxn.OnBubbleClickListener
import kotlinx.android.synthetic.main.activity_main.*
import mad.citybin.R
import mad.citybin.ui.main.BinsFragment
import mad.citybin.ui.main.DriversFragment
import mad.citybin.ui.main.HomeFragment
import mad.citybin.ui.main.SettingsFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, HomeFragment())
            .commit()

        tabBar.addBubbleListener(object : OnBubbleClickListener {
            override fun onBubbleClick(id: Int) {
                changePage(
                    when (id) {
                        R.id.menu_page_home -> HomeFragment()
                        R.id.menu_page_bins -> BinsFragment()
                        R.id.menu_page_drivers -> DriversFragment()
                        R.id.menu_page_settings -> SettingsFragment()
                        else -> HomeFragment()
                    }
                )

            }
        })


    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            super.onBackPressed()
        }
        supportFragmentManager.popBackStackImmediate()
        tabBar.setSelected(
            when (supportFragmentManager.findFragmentById(R.id.fragmentContainer)?.javaClass) {
                HomeFragment::class.java -> 0
                BinsFragment::class.java -> 1
                DriversFragment::class.java -> 2
                SettingsFragment::class.java -> 3
                else -> 0
            }
        )
    }

    fun changePage(fragment: Fragment) {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)

        // Prevent from adding the same fragment
        if (currentFragment?.javaClass == fragment.javaClass) {
            return;
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .addToBackStack(null)
            .commit()
    }

    fun setPage(index: Int) {
        changePage(
            when (index) {
                0 -> HomeFragment()
                1 -> BinsFragment()
                2 -> DriversFragment()
                3 -> SettingsFragment()
                else -> HomeFragment()
            }
        )
        tabBar.setSelected(index)
    }


}