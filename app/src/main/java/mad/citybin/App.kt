package mad.citybin

import android.app.Application
import android.text.TextUtils
import android.util.Log
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.facebook.drawee.backends.pipeline.Fresco

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this);
    }

}