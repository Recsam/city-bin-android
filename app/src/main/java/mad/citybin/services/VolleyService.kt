package mad.citybin.services

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import mad.citybin.R

object VolleyService {
    private val TAG: String = VolleyService::class.java.simpleName
    private var context: Context? = null
    private var basePath: String? = null

    // Make an instance of request queue
    private val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(context)
            }
            return field
        }

    // ClassName.invoke(args) = ClassName(args)
    operator fun invoke(context: Context?): VolleyService {
        this.context = context
        this.basePath = context?.getString(R.string.api_base_url)
        return this
    }

    fun get(
        path: String,
        completionHandler: (response: String?) -> Unit
    ) {
        addToRequestQueue(StringRequest(
            Request.Method.GET,
            basePath + path,
            Response.Listener { response ->
                Log.d(TAG, "/get request OK! Response: $response")
                completionHandler(response)
            },
            Response.ErrorListener { error ->
                VolleyLog.e(TAG, "/get request fail! Error: ${error.message}")
                completionHandler(null)
            }
        ), TAG)
    }

    fun post(
        path: String,
        data: MutableMap<String, String>,
        completionHandler: (response: String?) -> Unit
    ) {
        addToRequestQueue(object : StringRequest(
            Request.Method.POST, basePath + path,
            Response.Listener { response ->
                Log.d(TAG, "/post request OK! Response: $response")
                completionHandler(response)
            },
            Response.ErrorListener { error ->
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                completionHandler(null)
            }
        ) {

            override fun getParams(): MutableMap<String, String> {
                return data
            }

        }, TAG)
    }

    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }

    private fun addToRequestQueue(request: StringRequest, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue?.add(request)
    }

}