package mad.citybin.services

import android.content.Context
import com.google.gson.reflect.TypeToken
import mad.citybin.models.Area

class AreaService(context: Context?, override val baseUri: String = "/areas") : Service<Area>(context) {

    override fun getAll(handler: (data: Array<Area>) -> Unit) {
        volleyService.get(baseUri) {response ->
            val type = object : TypeToken<Array<Area>>() {}.type
            handler(gson.fromJson<Array<Area>>(response, type))
        }
    }

    override fun getById(id: String, handler: (data: Area) -> Unit) {
        volleyService.get("$baseUri/$id") { response ->
            handler(gson.fromJson(response, Area::class.java))
        }
    }

}