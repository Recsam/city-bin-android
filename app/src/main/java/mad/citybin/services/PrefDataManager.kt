package mad.citybin.services

import android.content.Context
import android.content.SharedPreferences

object PrefDataManager {
    private lateinit var preference: SharedPreferences

    operator fun invoke(context: Context): PrefDataManager {
        preference = context.getSharedPreferences("city-bin", 0) // 0 -> Private Mode
        return this
    }

    fun setUser(userId: String) {
        val editor = preference.edit()
        editor.putString("userId", userId)
        editor.apply()
    }

    fun removeUser() {
        val editor = preference.edit()
        editor.remove("userId")
        editor.apply()
    }

    fun getUser() : String? = preference.getString("userId", null)

}