package mad.citybin.services

import android.content.Context
import com.google.gson.reflect.TypeToken
import mad.citybin.models.Bin

class BinService(context: Context?, override val baseUri: String = "/bins") : Service<Bin>(context) {

    override fun getAll(handler: (data: Array<Bin>) -> Unit) {
        volleyService.get(baseUri) { response ->
            val type = object : TypeToken<Array<Bin>>() {}.type
            handler(gson.fromJson<Array<Bin>>(response, type))
        }
    }

    override fun getById(id: String, handler: (data: Bin) -> Unit) {
        volleyService.get("$baseUri/$id") { response ->
            handler(gson.fromJson(response, Bin::class.java))
        }
    }

    fun getUncleared(handler: (amount: Int) -> Unit) {
        volleyService.get(baseUri) {response ->
            val type = object : TypeToken<Array<Bin>>() {}.type
            handler(gson.fromJson<Array<Bin>>(response, type).filter { bin -> !bin.isCleared }.size)
        }
    }

    fun getAllWithAreaId(id: String, handler: (data: Array<Bin>) -> Unit) {
        volleyService.get("$baseUri?areaId=$id") {response ->
            val type = object : TypeToken<Array<Bin>>() {}.type
            handler(gson.fromJson<Array<Bin>>(response, type))
        }
    }

}
