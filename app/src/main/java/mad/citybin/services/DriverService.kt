package mad.citybin.services

import android.content.Context
import com.google.gson.reflect.TypeToken
import mad.citybin.models.Bin
import mad.citybin.models.Driver

class DriverService(context: Context?, override val baseUri: String = "/drivers") : Service<Driver>(context) {

    override fun getAll(handler: (data: Array<Driver>) -> Unit) {
        volleyService.get(baseUri) { response ->
            val type = object : TypeToken<Array<Driver>>() {}.type
            handler(gson.fromJson<Array<Driver>>(response, type))
        }
    }

    override fun getById(id: String, handler: (data: Driver) -> Unit) {
        volleyService.get("$baseUri/$id") { response ->
            handler(gson.fromJson(response, Driver::class.java))
        }
    }
}