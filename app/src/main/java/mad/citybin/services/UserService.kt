package mad.citybin.services

import android.content.Context
import com.google.gson.reflect.TypeToken
import mad.citybin.models.User

class UserService(context: Context?, override val baseUri: String = "/users") :
    Service<User>(context) {

    fun store(user: User, handler: (user: User?) -> Unit) {
        volleyService.post(
            baseUri,
            mutableMapOf(
                "id" to user.id,
                "username" to user.username,
                "email" to user.email,
                "password" to user.password
            )
        ) { response ->
            handler(gson.fromJson(response, User::class.java))
        }
    }

    override fun getAll(handler: (data: Array<User>) -> Unit) {
        volleyService.get(baseUri) { response ->
            val type = object : TypeToken<Array<User>>() {}.type
            handler(gson.fromJson<Array<User>>(response, type))
        }
    }

    override fun getById(id: String, handler: (data: User) -> Unit) {
        volleyService.get("$baseUri/$id") { response ->
            handler(gson.fromJson(response, User::class.java))
        }
    }

    fun getByEmail(email: String, handler: (data: User?) -> Unit) {
        volleyService.get("$baseUri?email=$email") { response ->
            val type = object : TypeToken<Array<User>>() {}.type
            val users = gson.fromJson<Array<User>>(response, type)
            if (users.isNotEmpty()) handler(users[0]) else handler(null)
        }
    }


}