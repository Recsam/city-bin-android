package mad.citybin.services

import android.content.Context
import com.google.gson.Gson

abstract class Service<T>(val context: Context?) {
    protected val volleyService = VolleyService(context)
    protected val gson = Gson()
    protected abstract val baseUri: String

    abstract fun getAll(handler: (data: Array<T>) -> Unit)
    abstract fun getById(id: String, handler: (data: T) -> Unit)
}